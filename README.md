# teaser-manager (Contao-Modul)

*erstellt für Contao 3.5.12, getestet bis Contao 3.5.27*

Blöcke (z.B. in der Seitenleiste) können zentral verwaltet und als Gruppen zusammen gestellt werden.

Es werden zum einen Teaser erstellt in denen man den Titel, einen Text, ein Bild, eine Verlinkung, ein Farbwert und aktiv/deaktiv setzen kann.

Dann werden diese Teaser in Teasergruppen gebündelt (Teaser können ausgewählt und sortiert werden) und entweder per Whitelist oder Blacklist zu Seiten  zugewiesen oder von Seiten ausgeschlossen. So kann man als Beispiel die Kontaktdatenbox auf jeder Seite erscheinen lassen außer auf der Kontaktseite selbst. Da die Daten da ja schon im Hauptinhalt auftauchen.

Dieser Teasergruppen können dann per Content-Element oder als Frontend-Modul auf der Website dargestellt werden.

***

Dieses Modul ist noch zeimlich einfach gehalten und hat noch wesentliches Ausbau-Potential. Bei Verbesserungs-wünschen gerne als Issue noch besser als Auftrag ;)
