<?php
/**
 * Created by sr-tag.de (contao 4.9).
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 19.06.23
 */

namespace Srhinow\TeaserManager\EventListener\Dca;

use Contao\Backend;

class TeaserGroup extends Backend
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Store the date when the account has been added
     *
     * @param DataContainer $dc
     */
    public function storeDateAdded($dc)
    {
        // Front end call
        if (!$dc instanceof DataContainer)
        {
            return;
        }

        // Return if there is no active record (override all)
        if (!$dc->activeRecord || $dc->activeRecord->dateAdded > 0)
        {
            return;
        }

        $time = time();

        $this->Database->prepare("UPDATE tl_teaser_group SET dateAdded=? WHERE id=?")
            ->execute($time, $dc->id);
    }

}