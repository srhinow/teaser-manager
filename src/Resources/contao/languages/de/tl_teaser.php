<?php
/**
 * Contao webCMS
 *
 * @copyright  Sven Rhinow 2015
 * @author     Sven Rhinow <sven@sr-tag.de>
 * @package    teaser-manager
 * @license    LGPL
 * @filesource
 */

/**
* Button
*/
$GLOBALS['TL_LANG']['tl_teaser']['new']    = array('Neuer Teaser', 'Einen neuen Teaser anlegen.');
$GLOBALS['TL_LANG']['tl_teaser']['edit']   = array('Teaser bearbeiten', 'Teaser ID %s bearbeiten');
$GLOBALS['TL_LANG']['tl_teaser']['copy']   = array('Teaser duplizieren', 'Teaser ID %s duplizieren');
$GLOBALS['TL_LANG']['tl_teaser']['delete'] = array('Teaser löschen', 'Teaser ID %s löschen');

/**
* Legends
*/
$GLOBALS['TL_LANG']['tl_teaser']['basicsettings_legend'] = 'Basis-Einstellungen';
$GLOBALS['TL_LANG']['tl_teaser']['content_legend'] = 'Text-Inhalte (<strong style="color:red;">veraltet!</strong> Bitte zugeh. Inhaltselemente anlegen)';
$GLOBALS['TL_LANG']['tl_teaser']['image_legend'] = 'Bild-Einstellungen (<strong style="color:red;">veraltet!</strong> Bitte zugeh. Inhaltselemente anlegen)';
$GLOBALS['TL_LANG']['tl_teaser']['link_legend'] = 'Link-Einstellungen (<strong style="color:red;">veraltet!</strong> Bitte zugeh. Inhaltselemente anlegen)';
$GLOBALS['TL_LANG']['tl_teaser']['template_legend'] = 'Template Einstellungen';
$GLOBALS['TL_LANG']['tl_teaser']['extend_legend'] = 'weitere Einstellungen';

/**
* Fields
*/
$GLOBALS['TL_LANG']['tl_teaser']['internTitle'][0] = 'interne Bezeichnung (Backend)';
$GLOBALS['TL_LANG']['tl_teaser']['internTitle'][1] = 'Diese Bezeichnung dient zur besseren Zuordnung und Filterung im Backend.';
$GLOBALS['TL_LANG']['tl_teaser']['headline'][0] = 'Überschrift';
$GLOBALS['TL_LANG']['tl_teaser']['headline'][1] = 'Titel/Überschrift im Teaser';
$GLOBALS['TL_LANG']['tl_teaser']['text'][0] = 'kurzer Teasertext';
$GLOBALS['TL_LANG']['tl_teaser']['text'][1] = 'Text im Teaser';
$GLOBALS['TL_LANG']['tl_teaser']['image'][0] = 'Bild';
$GLOBALS['TL_LANG']['tl_teaser']['image'][1] = 'Bild im Teaser';
$GLOBALS['TL_LANG']['tl_teaser']['linkTitle'][0] = 'Link-Titel';
$GLOBALS['TL_LANG']['tl_teaser']['linkTitle'][1] = 'der Text der zu weiteren Informationen verlinkt ist';
$GLOBALS['TL_LANG']['tl_teaser']['url'][0] = 'Link';
$GLOBALS['TL_LANG']['tl_teaser']['url'][1] = 'ein Link der zu weiteren Informationen führt.';
$GLOBALS['TL_LANG']['tl_teaser']['category'][0] = 'Kategorie';
$GLOBALS['TL_LANG']['tl_teaser']['category'][1] = 'Welche Kategorie gehört dieser Teaser an?';
$GLOBALS['TL_LANG']['tl_teaser']['teaser_color'][0] = 'Teaser-Farbe';
$GLOBALS['TL_LANG']['tl_teaser']['teaser_color'][1] = 'Wählen Sie die Hintergrundfarbe des Teaser aus.';
$GLOBALS['TL_LANG']['tl_teaser']['featured'][0] = 'hervorgehoben';
$GLOBALS['TL_LANG']['tl_teaser']['featured'][1] = 'Dient als Filter wenn z.B. auf der Startseite nur die Teaser die vorgehoben sind dargestellt werden soll.';
$GLOBALS['TL_LANG']['tl_teaser']['teaser_template'][0] = 'Teaser-Template zuweisen';
$GLOBALS['TL_LANG']['tl_teaser']['teaser_template'][1] = 'Wählen Sie ein passendes Template für diesen Teaser aus.';
$GLOBALS['TL_LANG']['tl_teaser']['active'][0] = 'aktiv';
$GLOBALS['TL_LANG']['tl_teaser']['active'][1] = '';
$GLOBALS['TL_LANG']['tl_teaser']['isLink'][0] = 'Link einfügen';
$GLOBALS['TL_LANG']['tl_teaser']['isLink'][1] = '';
$GLOBALS['TL_LANG']['tl_teaser']['isImage'][0] = 'Bild einfügen';
$GLOBALS['TL_LANG']['tl_teaser']['isImage'][1] = '';

/**
* Optionen (select-field)
*/
$GLOBALS['TL_LANG']['tl_teaser']['categories'] = array('kat1'=>'Kategorie 1','kat2'=>'Kategorie 2','kat3'=>'Kategorie 3');
$GLOBALS['TL_LANG']['tl_teaser']['color_options'] = array('blue'=>'blau','yellow'=>'gelb','green'=>'grün','grey'=>'grau');
