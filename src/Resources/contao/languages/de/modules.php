<?php

/**
 * 
 * Contao extension: teaser
 * Deutsch languange file 
 * 
 * @copyright  Sven Rhinow 2015
 * @author     Sven Rhinow <sven@sr-tag.de>
 * @package    teaserManager
 * @license    LGPL
 * 
 */

/**
 * Content elements
 */
$GLOBALS['TL_LANG']['CTE']['tm_teaser'] = array('Teaser', 'Stellt den ausgewählten Teaser dar');
$GLOBALS['TL_LANG']['CTE']['tm_teaser_group'] = array('Teaser-Gruppe', 'Stellt die ausgewählte Teaser-Gruppe dar');


/**
 * Front end modules
 */
$GLOBALS['TL_LANG']['FMD']['tm_teaser'] = array('Teaser', 'Stellt den ausgewählten Teaser dar');
$GLOBALS['TL_LANG']['FMD']['tm_teaser_group'] = array('Teaser-Gruppe', 'Stellt die ausgewählte Teaser-Gruppe dar');


/**
 * Back end modules
 */
 $GLOBALS['TL_LANG']['MOD']['tm_teaser']       = array('Teaser', 'Mit diesem Modul verwalten Sie die Teaser für die Frontenddarstellung.');
 $GLOBALS['TL_LANG']['MOD']['tm_teaser_group']       = array('Teaser-Gruppen', 'Mit diesem Modul verwalten Sie die Zuweisung von Teaser-Gruppen.');
