<?php

use Srhinow\TeaserManager\Controller\ContentElement\ContentTeaserController;
use Srhinow\TeaserManager\Controller\ContentElement\ContentTeaserGroupController;


/**
 * add Fields to tl_content
 */
$GLOBALS['TL_LANG']['tl_content'][ContentTeaserController::TYPE]          = array('Teaser', 'Bitte wählen Sie einen Teaser aus.');
$GLOBALS['TL_LANG']['tl_content'][ContentTeaserGroupController::TYPE]        = array('Teaser-Gruppe', 'Bitte wählen Sie eine Teaser-Gruppe aus.');
