<?php
/**
 * Contao webCMS
 *
 * @copyright  Sven Rhinow 2015
 * @author     Sven Rhinow <sven@sr-tag.de>
 * @package    teaser-manager
 * @license    LGPL
 * @filesource
 */

/**
* Button
*/
$GLOBALS['TL_LANG']['tl_teaser_group']['new']    = array('Neue Teaser-Gruppe', 'Eine neue Teaser-Gruppe anlegen.');
$GLOBALS['TL_LANG']['tl_teaser_group']['edit']   = array('Teaser-Gruppe bearbeiten', 'Teaser-Gruppe ID %s bearbeiten');
$GLOBALS['TL_LANG']['tl_teaser_group']['copy']   = array('Teaser-Gruppe duplizieren', 'Teaser-Gruppe ID %s duplizieren');
$GLOBALS['TL_LANG']['tl_teaser_group']['delete'] = array('Teaser-Gruppe löschen', 'Teaser-Gruppe ID %s löschen');

/**
* Fields
*/
$GLOBALS['TL_LANG']['tl_teaser_group']['internTitle'][0] = 'interne Bezeichnung (Backend)';
$GLOBALS['TL_LANG']['tl_teaser_group']['internTitle'][1] = 'Diese Bezeichnung dient zur besseren Zuordnung und Filterung im Backend.';
$GLOBALS['TL_LANG']['tl_teaser_group']['inContainer'][0] = 'Anzeigebereich';
$GLOBALS['TL_LANG']['tl_teaser_group']['inContainer'][1] = 'Wählen sie den Anzeigebereich der zur Filterung im Frontendmodul dient aus.';
$GLOBALS['TL_LANG']['tl_teaser_group']['teaser'][0] = 'Teaser';
$GLOBALS['TL_LANG']['tl_teaser_group']['teaser'][1] = 'Wählen Sie die benötigten Teaser aus. Die Sortierung findet durch das verschieben auf dem grünen Icon per gedürckter Maustaste statt.';
$GLOBALS['TL_LANG']['tl_teaser_group']['custom_pages'][0] = 'nur bestimmte Seiten';
$GLOBALS['TL_LANG']['tl_teaser_group']['custom_pages'][1] = 'Wenn die Teasergruppe nicht überall angezeigt werden soll, sondern nur auf bestimmmten Seiten, dann bestätigen Sie die Checkbox.';
$GLOBALS['TL_LANG']['tl_teaser_group']['pages'][0] = 'Seiten';
$GLOBALS['TL_LANG']['tl_teaser_group']['pages'][1] = 'Wählen Sie die die Seiten aus auf der die Teaser-Zusammenstellung angezeigt werden soll.';
$GLOBALS['TL_LANG']['tl_teaser_group']['exceptions'][0] = 'Ausnahmen definieren';
$GLOBALS['TL_LANG']['tl_teaser_group']['exceptions'][1] = 'Wenn die Teasergruppe auf bestimmten Seiten nicht angezeigt werden soll, dann bestätigen Sie die Checkbox.';
$GLOBALS['TL_LANG']['tl_teaser_group']['exceptions_pages'][0] = 'Seiten (ausgeschlossen)';
$GLOBALS['TL_LANG']['tl_teaser_group']['exceptions_pages'][1] = 'Wählen Sie die die Seiten aus auf der die Teaser-Zusammenstellung "NICHT" angezeigt werden soll.';

$GLOBALS['TL_LANG']['tl_teaser_group']['featured'][0] = 'hervorgehoben';
$GLOBALS['TL_LANG']['tl_teaser_group']['featured'][1] = 'Dient als Filter wenn z.B. auf der Startseite nur die Teaser die vorgehoben sind dargestellt werden soll.';
$GLOBALS['TL_LANG']['tl_teaser_group']['active'][0] = 'aktiv';
$GLOBALS['TL_LANG']['tl_teaser_group']['active'][1] = '';

/**
* Legends
*/
$GLOBALS['TL_LANG']['tl_teaser_group']['basicsettings_legend'] = 'Grund-Einstellungen';
$GLOBALS['TL_LANG']['tl_teaser_group']['pages_legend'] = 'Seiten-Einstellungen';
$GLOBALS['TL_LANG']['tl_teaser_group']['extend_legend'] = 'weitere Einstellungen';
/**
* Optionen (select-field)
*/
// $GLOBALS['TL_LANG']['tl_teaser_group']['categories'] = array('kat1'=>'Kategorie 1','kat2'=>'Kategorie 2','kat3'=>'Kategorie 3');
$GLOBALS['TL_LANG']['tl_teaser_group']['container_options'] = array('sidebar'=>'Seitenleiste','msgbar'=>'Hinweisbereich','main_teaser'=>'Teaser im Hauptbereich');
