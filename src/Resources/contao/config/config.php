<?php

/**
 *
 * PHP version 8
 * @copyright  sr-tag.de 2023
 * @author     Sven Rhinow
 * @package    teaser-manager
 * @license    LGPL
 * @filesource
 */

$GLOBALS['TEASERMANAGER_PUBLIC_FOLDER'] = 'bundles/srhinowteasermanager';
/**
 * Add back end modules
 */
$teaser = array
	(
		'tm_teaser' => array
		(
			'tables' => array('tl_teaser', 'tl_content'),
			'icon'   => $GLOBALS['TEASERMANAGER_PUBLIC_FOLDER'].'/icons/column_single.png'
		),
		'tm_teaser_group' => array
		(
			'tables' => array('tl_teaser_group'),
			'icon'   => $GLOBALS['TEASERMANAGER_PUBLIC_FOLDER'].'/icons/combo_boxes.png'
		)
	);

array_insert($GLOBALS['BE_MOD']['content'], 1, $teaser);

/**
 * Add content element
 */
//$GLOBALS['TL_CTE']['includes']['tm_teaser'] = 'ContentTeaser';
//$GLOBALS['TL_CTE']['includes']['tm_teaser_group'] = 'ContentTeaserGroup';

/**
 * Front end modules
 */
//$GLOBALS['FE_MOD']['teaser-manager'] = array
//	(
//		// 'tm_teaser'   => 'ModuleTmTeaser',
//		'tm_teaser_group' => 'srhinow.teaser_manager.modules.frontend.module_teaser_group'
//	);

/*
 * -------------------------------------------------------------------------
 * MODELS
 * -------------------------------------------------------------------------
 */
$GLOBALS['TL_MODELS']['tl_teaser'] = \Srhinow\TeaserManager\Model\TeaserModel::class;
$GLOBALS['TL_MODELS']['tl_teaser_group'] = \Srhinow\TeaserManager\Model\TeaserGroupModel::class;
