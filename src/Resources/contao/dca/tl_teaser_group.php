<?php

/**
 *
 * @copyright  Sven Rhinow 2015
 * @author     Sven Rhinow <sven@sr-tag.de>
 * @package    teaser-manager
 * @license    LGPL
 */

/**
 * Table tl_teaser_group
 */
$GLOBALS['TL_DCA']['tl_teaser_group'] = array
(
	// Config
	'config' => array
	(
		'dataContainer'               => 'Table',
		'enableVersioning'            => true,
		'onsubmit_callback' => array
		(
			array('srhinow.teaser_manager.listener.dca.teaser_group', 'storeDateAdded')
		),
		'sql' => array
		(
			'keys' => array
			(
				'id' => 'primary',
				'active' => 'index'
			)
		)
	),
	// List
	'list' => array
	(
		'sorting' => array
		(
			'mode'                    => 2,
			'fields'                  => array('internTitle DESC'),
			'flag'                    => 1,
			'panelLayout'             => 'filter;sort;search,limit'
		),
		'label' => array
		(
			'fields'                  => array('internTitle', 'dateAdded'),
			'showColumns'             => true,
		),
		'global_operations' => array
		(
			'all' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
				'href'                => 'act=select',
				'class'               => 'header_edit_all',
				'attributes'          => 'onclick="Backend.getScrollOffset()" accesskey="e"'
			)
		),
		'operations' => array
		(
			'edit' => array
			(
				'href'                => 'act=edit',
				'icon'                => 'edit.gif'
			),
			'copy' => array
			(
				'href'                => 'act=copy',
				'icon'                => 'copy.gif'
			),
			'delete' => array
			(
				'href'                => 'act=delete',
				'icon'                => 'delete.gif',
				'attributes'          => 'onclick="if(!confirm(\'' . ($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null) . '\'))return false;Backend.getScrollOffset()"'
			),
		)
	),

	// Palettes
	'palettes' => array
	(
		'__selector__'				 => array('custom_pages','exceptions'),
		'default'                     => '{basicsettings_legend},internTitle,inContainer,teaser;{pages_legend},custom_pages,exceptions;{extend_legend},active,featured',
	),
	'subpalettes' => array
	(
		'custom_pages' => 'pages',
		'exceptions' => 'exceptions_pages'
	),


	// Fields
	'fields' => array
	(
		'id' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL auto_increment"
		),
		'tstamp' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'dateAdded' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['MSC']['dateAdded'],
			'sorting'                 => true,
			'flag'                    => 6,
			'eval'                    => array('rgxp'=>'datim'),
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'internTitle' => array
		(
			'exclude'                 => true,
			'search'                  => true,
			'sorting'                 => true,
			'flag'                    => 1,
			'inputType'               => 'text',
			'eval'                    => array('mandatory'=>true, 'maxlength'=>255, 'feEditable'=>true, 'feViewable'=>true, 'feGroup'=>'personal', 'tl_class'=>'long'),
			'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'inContainer' => array
		(
			'exclude'                 => true,
			'filter'                  => true,
			'sorting'                 => true,
			'inputType'               => 'select',
			'options'				  => ['sidebar','msgbar','main_teaser'],
			'reference'				  => &$GLOBALS['TL_LANG']['tl_teaser_group']['container_options'],
			'eval'                    => array('mandatory'=>true,'includeBlankOption'=>true),
			'sql'                     => "varchar(55) NOT NULL default ''"
		),
		'teaser' => array
		(
			'exclude'                 => true,
			'filter'                  => true,
			'search'                  => true,
			'sorting'                 => true,
			'flag'                    => 1,
			'inputType'               => 'checkboxWizard',
			'foreignKey'              => 'tl_teaser.internTitle',
			'eval'                    => array('multiple'=>true),
			'sql'                     => "blob NULL",
			'relation'                => array('type'=>'belongsToMany', 'load'=>'lazy')
		),
		'custom_pages' => array
		(
			'exclude'                 => true,
			'filter'                  => true,
			'inputType'               => 'checkbox',
			'eval'                    => array('submitOnChange'=>true),
			'sql'                     => "char(1) NOT NULL default ''",
		),
		'pages' => array
		(
			'exclude'                 => true,
			'filter'                  => true,
			'search'                  => true,
			'sorting'                 => true,
			'flag'                    => 1,
			'inputType'               => 'pageTree',
			'foreignKey'              => 'tl_page.title',
			'eval'                    => array('mandatory'=>true, 'fieldType'=>'checkbox','multiple'=>true),
			'sql'                     => "blob NULL",
			'relation'                => array('type'=>'belongsTo', 'load'=>'lazy')
		),
		'exceptions' => array
		(
			'exclude'                 => true,
			'filter'                  => false,
			'inputType'               => 'checkbox',
			'eval'                    => array('submitOnChange'=>true),
			'sql'                     => "char(1) NOT NULL default ''",
		),
		'exceptions_pages' => array
		(
			'exclude'                 => true,
			'filter'                  => true,
			'search'                  => true,
			'sorting'                 => true,
			'flag'                    => 1,
			'inputType'               => 'pageTree',
			'foreignKey'              => 'tl_page.title',
			'eval'                    => array('mandatory'=>true, 'fieldType'=>'checkbox','multiple'=>true),
			'sql'                     => "blob NULL",
			'relation'                => array('type'=>'belongsTo', 'load'=>'lazy')
		),
		'featured' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_teaser_group']['featured'],
			'exclude'                 => true,
			'filter'                  => true,
			'inputType'               => 'checkbox',
			'eval'                    => array('tl_class'=>'w50'),
			'sql'                     => "char(1) NOT NULL default ''"
		),
		'active' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_teaser_group']['active'],
			'exclude'                 => true,
			'filter'                  => true,
			'inputType'               => 'checkbox',
			'sql'                     => "char(1) NOT NULL default ''"
		),
	)
);
