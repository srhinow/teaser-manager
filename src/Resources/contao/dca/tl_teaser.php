<?php

/**
 *
 * @copyright  Sven Rhinow 2015
 * @author     Sven Rhinow <sven@sr-tag.de>
 * @package    teaser-manager
 * @license    LGPL
 */


/**
 * Table tl_teaser
 */
$GLOBALS['TL_DCA']['tl_teaser'] = array
(
	// Config
	'config' => array
	(
		'dataContainer'               => 'Table',
        'ctable'                      => array('tl_content'),
		'enableVersioning'            => true,
        'switchToEdit'                => true,
		'onsubmit_callback' => array
		(
			array('srhinow.teaser_manager.listener.dca.teaser', 'storeDateAdded')
		),

		'sql' => array
		(
			'keys' => array
			(
				'id' => 'primary',
				'active' => 'index'
			)
		)
	),

	// List
	'list' => array
	(
		'sorting' => array
		(
			'mode'                    => 2,
			'fields'                  => array('internTitle DESC'),
			'flag'                    => 1,
			'panelLayout'             => 'filter;sort,search,limit'
		),
		'label' => array
		(
			'fields'                  => array('internTitle', 'dateAdded','active'),
			'showColumns'             => true,
		),
		'global_operations' => array
		(
			'all' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
				'href'                => 'act=select',
				'class'               => 'header_edit_all',
				'attributes'          => 'onclick="Backend.getScrollOffset()" accesskey="e"'
			)
		),
		'operations' => array
		(
            'edit' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_theme_section_article']['edit'],
                'href'                => 'table=tl_content',
                'icon'                => 'edit.svg',
            ),
		    'editheader' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_teaser']['editheader'],
				'href'                => 'act=edit',
				'icon'                => 'header.svg'
			),
			'copy' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_teaser']['copy'],
				'href'                => 'act=copy',
				'icon'                => 'copy.gif'
			),
			'delete' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_teaser']['delete'],
				'href'                => 'act=delete',
				'icon'                => 'delete.gif',
				'attributes'          => 'onclick="if(!confirm(\'' . ($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null) . '\'))return false;Backend.getScrollOffset()"'
			),
		)
	),

	// Palettes
	'palettes' => array
	(
		'__selector__'				  => array('isLink','isImage'),
		'default'                     => '{basicsettings_legend},internTitle;{content_legend:hide},headline,text,teaser_color;{image_legend:hide},isImage;{link_legend:hide},isLink;{template_legend:hide},teaser_template;{extend_legend:hide},featured,cssID;active',
	),
	'subpalettes' => array
	(
		'isLink' 	=> 'url,linkTitle',
		'isImage'	=> 'image',
	),

	// Fields
	'fields' => array
	(
		'id' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL auto_increment"
		),
		'tstamp' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),

		'dateAdded' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['MSC']['dateAdded'],
			'sorting'                 => true,
			'flag'                    => 6,
			'eval'                    => array('rgxp'=>'datim'),
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'internTitle' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_teaser']['internTitle'],
			'exclude'                 => true,
			'search'                  => true,
			'sorting'                 => true,
			'flag'                    => 1,
			'inputType'               => 'text',
			'eval'                    => array('mandatory'=>true, 'maxlength'=>255, 'feEditable'=>true, 'feViewable'=>true, 'feGroup'=>'personal', 'tl_class'=>'w50'),
			'sql'                     => "varchar(255) NOT NULL default ''"
		),
        'cssID' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_theme_section_article']['cssID'],
            'exclude'                 => true,
            'inputType'               => 'text',
            'eval'                    => array('multiple'=>true, 'size'=>2, 'tl_class'=>'w50 clr'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
		'headline' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_teaser']['headline'],
			'exclude'                 => true,
			'search'                  => true,
			'sorting'                 => true,
			'flag'                    => 1,
			'inputType'               => 'text',
			'eval'                    => array('mandatory'=>false, 'maxlength'=>255,'tl_class'=>'long'),
			'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'text' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_teaser']['text'],
			'exclude'                 => true,
			'search'                  => true,
			'filter'                  => false,
			'inputType'               => 'textarea',
			'eval'                    => array('mandatory'=>false, 'cols'=>'10','rows'=>'10','rte'=>'tinyMCE'),
			'sql'					=> "blob NULL"

		),
		'isImage' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_teaser']['isImage'],
			'exclude'                 => true,
			'filter'                  => false,
			'inputType'               => 'checkbox',
			'eval'                    => array('submitOnChange'=>true),
			'sql'                     => "char(1) NOT NULL default ''",
		),
		'image' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_teaser']['image'],
			'exclude'                 => true,
			'inputType'               => 'fileTree',
			'eval'                    => array('filesOnly'=>true, 'extensions'=>Config::get('validImageTypes'), 'fieldType'=>'radio', 'mandatory'=>false),
			'sql'                     => "binary(16) NULL"
		),
		'isLink' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_teaser']['isLink'],
			'exclude'                 => true,
			'filter'                  => false,
			'inputType'               => 'checkbox',
			'eval'                    => array('submitOnChange'=>true),
			'sql'                     => "char(1) NOT NULL default ''",
		),
		'linkTitle' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_teaser']['linkTitle'],
			'exclude'                 => true,
			'default'				=> 'Alle Informationen',
			'inputType'               => 'text',
			'eval'                    => array('mandatory'=>true, 'maxlength'=>255, 'feEditable'=>true, 'feViewable'=>true, 'feGroup'=>'personal', 'tl_class'=>'w50'),
			'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'url' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_teaser']['url'],
			'exclude'                 => true,
			'search'                  => true,
			'inputType'               => 'text',
			'eval'                    => array('mandatory'=>true, 'decodeEntities'=>true, 'maxlength'=>255, 'fieldType'=>'radio', 'filesOnly'=>true, 'tl_class'=>'w50 wizard'),
			'wizard' => array
			(
				array('srhinow.teaser_manager.listener.dca.teaser', 'pagePicker')
			),
			'sql'                     => "varchar(255) NOT NULL default ''"
		),

		'category' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_teaser']['category'],
			'exclude'                 => true,
			// 'filter'                  => true,
			// 'sorting'                 => true,
			'inputType'               => 'select',
			'options'                 => &$GLOBALS['TL_LANG']['tl_teaser']['categories'],
			'eval'                    => array('mandatory'=>true,'includeBlankOption'=>true, 'chosen'=>true, 'feEditable'=>true, 'feViewable'=>true, 'tl_class'=>'clr w50'),
			'sql'                     => "varchar(25) NOT NULL default ''"
		),
		'teaser_color' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_teaser']['teaser_color'],
			'exclude'                 => true,
			'filter'                  => true,
			'sorting'                 => true,
			'inputType'               => 'select',
			'options'                 => &$GLOBALS['TL_LANG']['tl_teaser']['color_options'],
			'eval'					=> array('includeBlankOption'=>true, 'tl_class'=>'w50'),
			'sql'					=> "varchar(25) NOT NULL default ''"
		),
        'teaser_template' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_teaser']['teaser_template'],
            'default'                 => 'tm_teaser_default',
            'exclude'                 => true,
            'inputType'               => 'select',
            'options'	        	  => Backend::getTemplateGroup('tm_teaser_'),
            'eval'                    => array('tl_class'=>'w50'),
            'sql'                     => "varchar(32) NOT NULL default ''"
        ),
		'featured' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_teaser']['featured'],
			'exclude'                 => true,
			'filter'                  => true,
			'inputType'               => 'checkbox',
			'eval'                    => array('tl_class'=>'w50'),
			'sql'                     => "char(1) NOT NULL default ''"
		),
		'active' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_teaser']['active'],
			'exclude'                 => true,
			'filter'                  => true,
			'inputType'               => 'checkbox',
			'sql'                     => "char(1) NOT NULL default ''"
		),
	)
);

