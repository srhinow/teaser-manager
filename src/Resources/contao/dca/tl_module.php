<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Leo Feyer
 *
 * @license LGPL-3.0+
 */

$this->loadLanguageFile('tl_teaser_group');

/**
 * Add palettes to tl_module
 */
$GLOBALS['TL_DCA']['tl_module']['palettes']['teaser_group_module']   = '{title_legend},name,headline,type;{config_legend},tm_inContainer;{template_legend:hide},customTpl;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID,space';


/**
 * Add fields to tl_module
 */
$GLOBALS['TL_DCA']['tl_module']['fields']['tm_teaser_groups'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['tm_teaser_groups'],
	'exclude'                 => true,
	'inputType'               => 'select',
	'foreignKey'              => 'tl_teaser_group.internTitle',
	'eval'                    => array('mandatory'=>true,'includeBlankOption'=>true),
	'sql'                     => "int(10) unsigned NOT NULL default '0'"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['tm_inContainer'] = array
(
			'label'                   => &$GLOBALS['TL_LANG']['tl_teaser_group']['inContainer'],
			'exclude'                 => true,
			'filter'                  => true,
			'sorting'                 => true,
			'inputType'               => 'select',
			'options'				  => &$GLOBALS['TL_LANG']['tl_teaser_group']['container_options'],
			'eval'                    => array('mandatory'=>true,'includeBlankOption'=>true),
			'sql'                     => "varchar(55) NOT NULL default ''"
);


