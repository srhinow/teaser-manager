<?php

use Srhinow\TeaserManager\Controller\ContentElement\ContentTeaserController;
use Srhinow\TeaserManager\Controller\ContentElement\ContentTeaserGroupController;

/**
 * Dynamically add the parent table
 */
if (Input::get('do') == 'tm_teaser')
{
    $GLOBALS['TL_DCA']['tl_content']['config']['ptable'] = 'tl_teaser';
}

$GLOBALS['TL_DCA']['tl_content']['palettes'][ContentTeaserController::TYPE] = '{type_legend},type,title;{include_legend},tm_teaser,tm_teaser_template;{template_legend:hide},customTpl;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID,space;{invisible_legend:hide},invisible,start,stop';
$GLOBALS['TL_DCA']['tl_content']['palettes'][ContentTeaserGroupController::TYPE] = '{type_legend},type,title;{include_legend},tm_teaser_group,tm_teaser_template;{template_legend:hide},customTpl;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID,space;{invisible_legend:hide},invisible,start,stop';

/**
 * Add fields to tl_content
 */
$GLOBALS['TL_DCA']['tl_content']['fields']['tm_teaser'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_content']['tm_teaser'],
	'exclude'                 => true,
	'inputType'               => 'select',
	'options_callback'        => array('tl_tm_content', 'getTeaser'),
	'reference'               => &$GLOBALS['TL_LANG']['MSC'],
	'eval'                    => array('mandatory'=>true, 'chosen'=>true),
	'sql'                     => "int(10) unsigned NOT NULL default '0'"
);

$GLOBALS['TL_DCA']['tl_content']['fields']['tm_teaser_group'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_content']['tm_teaser_group'],
	'exclude'                 => true,
	'inputType'               => 'select',
	'options_callback'        => array('tl_tm_content', 'getTeaserGroup'),
	'reference'               => &$GLOBALS['TL_LANG']['MSC'],
	'eval'                    => array('mandatory'=>true, 'chosen'=>true),
	'sql'                     => "int(10) unsigned NOT NULL default '0'"
);

$GLOBALS['TL_DCA']['tl_content']['fields']['tm_teaser_template'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_teaser_group']['teaser_template'],
	'default'                 => 'com_default',
	'exclude'                 => true,
	'inputType'               => 'select',
	'options'	        	  => Backend::getTemplateGroup('teaser_'),
	'eval'                    => array('tl_class'=>'w50'),
	'sql'                     => "varchar(32) NOT NULL default ''"
);
/**
 * Provide miscellaneous methods that are used by the data configuration array.
 *
 * @author Sven Rhinow <sven@sr-tag.de>
 */
class tl_tm_content extends Backend
{
	/**
	 * Get all teaser and return them as array
	 *
	 * @return array
	 */
	public function getTeaser()
	{
		$arrTeaser = array();
		$objTeaser = $this->Database->prepare("SELECT * FROM tl_teaser WHERE active=?")->execute(1);

		while ($objTeaser->next())
		{
			$arrTeaser[$objTeaser->id] = $objTeaser->internTitle . ' (ID ' . $objTeaser->id . ')';
		}

		return $arrTeaser;
	}

	/**
	 * Get all teaser-groups and return them as array
	 *
	 * @return array
	 */
	public function getTeaserGroup()
	{
		$arrTeaserGroup = array();
		$objTeaserGroup = $this->Database->prepare("SELECT * FROM tl_teaser_group WHERE active=?")->execute(1);

		while ($objTeaserGroup->next())
		{
			$arrTeaserGroup[$objTeaserGroup->id] = $objTeaserGroup->internTitle . ' (ID ' . $objTeaserGroup->id . ')';
		}

		return $arrTeaserGroup;
	}
}
