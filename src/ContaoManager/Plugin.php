<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/teaser-manager
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/teaser-manager
 */

namespace Srhinow\TeaserManager\ContaoManager;

use Contao\CoreBundle\ContaoCoreBundle;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Srhinow\TeaserManager\SrhinowTeaserManager;

/**
 * Plugin for the Contao Manager.
 */
class Plugin implements BundlePluginInterface
{
    /**
     * {@inheritdoc}
     */
    public function getBundles(ParserInterface $parser)
    {
        return [
            BundleConfig::create(SrhinowTeaserManager::class)
                ->setLoadAfter([ContaoCoreBundle::class]),
        ];
    }
}
