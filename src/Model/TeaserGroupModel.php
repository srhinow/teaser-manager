<?php

namespace Srhinow\TeaserManager\Model;
use Contao\Model;

/**
 * 
 * @copyright  Sven Rhinow 2015
 * @author     Sven Rhinow <sven@sr-tag.de>
 * @package    teaser-manager
 * @license    LGPL
 * 
 * read and write teaser-group
 */
class TeaserGroupModel extends Model
{

	/**
	 * Table name
	 * @var string
	 */
	protected static $strTable = 'tl_teaser_group';

	/**
	 * Find all published FAQs by their parent IDs
	 *
	 * @param string $container  An Name from Container
	 * @param array $arrOptions An optional options array
	 *
	 * @return \Model\Collection|\FaqModel|null A collection of models or null if there are no FAQs
	 */
	public static function findActiveTeaserGroupsByContainer($container, array $arrOptions=array())
	{
		$t = static::$strTable;
		
		$arrColumns = array();

		if (!BE_USER_LOGGED_IN)
		{
			$arrColumns[] = "$t.active='1'";
			$arrColumns[] = "$t.inContainer='".$container."'";
		}

		if (!isset($arrOptions['order']))
		{
			$arrOptions['order'] = "$t.featured DESC";
		}

		return static::findBy($arrColumns, null, $arrOptions);
	}

}
