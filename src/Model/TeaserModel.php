<?php

namespace Srhinow\TeaserManager\Model;

class TeaserModel extends \Model
{

	/**
	 * Table name
	 * @var string
	 */
	protected static $strTable = 'tl_teaser';

}
