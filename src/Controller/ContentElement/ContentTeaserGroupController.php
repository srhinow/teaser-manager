<?php

declare(strict_types=1);

namespace Srhinow\TeaserManager\Controller\ContentElement;

use Contao\ContentModel;
use Contao\CoreBundle\Controller\ContentElement\AbstractContentElementController;
use Contao\Template;
use Srhinow\TeaserManager\Controller\Trait\TeaserManagerTrait;
use Srhinow\TeaserManager\Model\TeaserGroupModel;
use Srhinow\TeaserManager\Model\TeaserModel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ContentTeaserGroupController extends AbstractContentElementController
{
    use TeaserManagerTrait;

    public const TYPE = 'tm_teaser_group';

    protected function getResponse(Template $template, ContentModel $model, Request $request): Response
    {
        $tmObj = TeaserGroupModel::findById($model->tm_teaser_group);

        $template->teaser = $this->getHtmlFromTeaserGroup($tmObj->teaser, $model->tm_teaser_template);

        return $template->getResponse();
    }
}
