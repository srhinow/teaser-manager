<?php

declare(strict_types=1);

namespace Srhinow\TeaserManager\Controller\ContentElement;

use Contao\CoreBundle\Controller\ContentElement\AbstractContentElementController;
use Contao\ContentModel;
use Contao\Template;
use Srhinow\TeaserManager\Controller\Trait\TeaserManagerTrait;
use Srhinow\TeaserManager\Model\TeaserModel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ContentTeaserController extends AbstractContentElementController
{
    use TeaserManagerTrait;

    public const TYPE = 'tm_teaser';

    protected function getResponse(Template $template, ContentModel $model, Request $request): Response
    {
        $tmObj = TeaserModel::findById($model->tm_teaser);
        $template->teaser = $this->getTeaserHtml($tmObj);

        return $template->getResponse();
    }
}
