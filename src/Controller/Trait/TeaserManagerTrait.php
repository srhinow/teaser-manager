<?php

declare(strict_types=1);


namespace Srhinow\TeaserManager\Controller\Trait;

use Contao\ContentModel;
use Contao\Controller;
use Contao\FilesModel;
use Contao\FrontendTemplate;
use Contao\StringUtil;
use Srhinow\TeaserManager\Model\TeaserGroupModel;
use Srhinow\TeaserManager\Model\TeaserModel;

trait TeaserManagerTrait
{
    /**
     * Get teaser groups by page-id and target container
     */
    public function getTeaserGroupsByPage(int $id, string $container): array
    {
        $groups = $pageArr = $pageExceptArr = array();

        if(!$id || !$container) return $groups;

        $groupObj = TeaserGroupModel::findActiveTeaserGroupsByContainer($container);

        if(!is_object($groupObj)) return $groups;

        while($groupObj->next() )
        {
            if($groupObj->custom_pages && null !== $groupObj->pages)
            {
                // aufnehmen nur wenn es dieser Seite zugewiesen wurde
                $pageArr = StringUtil::deserialize($groupObj->pages);
                if(in_array($id, $pageArr)) $groups[$groupObj->id] = $groupObj->teaser;
            }
            else
            {
                // alle aufnehmen
                $groups[$groupObj->id] = $groupObj->teaser;
            }

            //Ausnahmen wieder loeschen
            if($groupObj->exceptions && null !== $groupObj->exceptions_pages)
            {
                $pageExceptArr = StringUtil::deserialize($groupObj->exceptions_pages);
                if(in_array($id,$pageExceptArr)) unset($groups[$groupObj->id]);
            }
        }

        return $groups;
    }

    /**
     * holt das HTML von einer teaser-Gruppe
     */
    public function getHtmlFromTeaserGroup(string $teaser, string $customTemplate = ''): string
    {
        $teaserArr = StringUtil::deserialize($teaser);
        $teaserStr = '';

        if (!is_array($teaserArr)) return $teaserStr;

        foreach($teaserArr as $teaserId)
        {
            $tObj = TeaserModel::findById($teaserId);
            if($tObj->active == 1) $teaserStr .= $this->getTeaserHtml($tObj, $customTemplate);
        }

        return $teaserStr;
    }

    /**
     * holt das HTML von einem teaser
     */
    public function getTeaserHtml(object $tObj, string $customTemplate=''): string
    {
        global $objPage;
        $teaserStr = '';

        $template='tm_teaser_default';

        // wenn direkt in dem Teaser-Einstellungen ein Template gesetzt wurde
        if((strlen($tObj->teaser_template) > 1)) $template = $tObj->teaser_template;

        //wenn z.B. per ContentElement gesetzt wurde dieses nehmen
        if(strlen($customTemplate) > 1) $template = $customTemplate;

        $objPartial = new FrontendTemplate($template);


        //Beginn deprecated
        $objPartial->text = $tObj->text;

        //image-path from binary
        if($tObj->isImage)
        {
            $objFile = FilesModel::findByUuid($tObj->image);

            if ($objFile !== null)
            {
                $objPartial->image = $objFile->path;
            }
        }

        $arrCssID = StringUtil::deserialize($tObj->cssID);
        list($strId, $strClasses) = $arrCssID;

        if((int) $tObj->featured === 1) $strClasses .= ' featured';

        $objPartial->isImage = $tObj->isImage;
        $objPartial->headline = $tObj->headline;
        $objPartial->isLink = $tObj->isLink;
        $objPartial->linkTitle = $tObj->linkTitle;
        $objPartial->id = $strId;
        $objPartial->css = $strClasses;
        $objPartial->url = $tObj->url;
        $objPartial->teaser_color = $tObj->teaser_color;
        // END deprecated

        $objPartial->content = $this->getTeaserContentHTML($tObj);
        $teaserStr .= $objPartial->parse();

        return $teaserStr;
    }

    protected function getTeaserContentHTML(object $tObj): string
    {
        $arrElements = array();
        $objCte = ContentModel::findPublishedByPidAndTable($tObj->id, 'tl_teaser');

        if ($objCte !== null)
        {
            $intCount = 0;
            $intLast = $objCte->count() - 1;

            while ($objCte->next())
            {
                $arrCss = array();

                /** @var ContentModel $objRow */
                $objRow = $objCte->current();

                // Add the "first" and "last" classes (see #2583)
                if ($intCount == 0 || $intCount == $intLast)
                {
                    if ($intCount == 0)
                    {
                        $arrCss[] = 'first';
                    }

                    if ($intCount == $intLast)
                    {
                        $arrCss[] = 'last';
                    }
                }

                $objRow->classes = $arrCss;
                $arrElements[] = Controller::getContentElement($objRow);
                ++$intCount;
            }
        }

        return implode('',$arrElements);
    }
}
