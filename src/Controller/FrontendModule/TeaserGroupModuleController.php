<?php

declare(strict_types=1);


namespace Srhinow\TeaserManager\Controller\FrontendModule;

use Contao\CoreBundle\Controller\FrontendModule\AbstractFrontendModuleController;
use Contao\ModuleModel;
use Contao\Template;
use Srhinow\TeaserManager\Controller\Trait\TeaserManagerTrait;
use Srhinow\TeaserManager\Helper\TeaserManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TeaserGroupModuleController extends AbstractFrontendModuleController
{
    use TeaserManagerTrait;

    protected function getResponse(Template $template, ModuleModel $model, Request $request): Response
    {
        $objPage = $this->getPageModel();

        $teaserHtml = '';
        $teaserGroups = $this->getTeaserGroupsByPage((int) $objPage->id, $model->tm_inContainer);
        foreach($teaserGroups as $groupId => $teaser)
        {
            $teaserHtml .= $this->getHtmlFromTeaserGroup($teaser);

        }
        $template->teaser = $teaserHtml;

        return $template->getResponse();
    }
}
