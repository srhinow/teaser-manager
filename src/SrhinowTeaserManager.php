<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/teaser-manager
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/teaser-manager
 */

namespace Srhinow\TeaserManager;

use Srhinow\TeaserManager\DependencyInjection\SrhinowTeaserManagerExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Configures the Contao teaser-manager bundle.
 */
class SrhinowTeaserManager extends Bundle
{
    /**
     * Builds the bundle.
     *
     * It is only ever called once when the cache is empty.
     *
     * This method can be overridden to register compilation passes,
     * other extensions, ...
     *
     * @param ContainerBuilder $container A ContainerBuilder instance
     */
    public function build(ContainerBuilder $container): void
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getContainerExtension()
    {
        return new SrhinowTeaserManagerExtension();
    }
}
