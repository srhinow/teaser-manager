# Changes in srhinow/teaser-manager contao-bundle

# 2.5.1 (14.05.2024)
- fix: give pageID as integer attribute

# 2.5 (14.05.2024)
- code-refactoring: frontend-modules and content-elements to Controller, Helper to Trait
- change require to >= PHP 8.2, >= Contao 4.13

## 2.4 (13.05.2024)
- fix dca confirm php9 issue
- add use in Teaser.php
- remove Patchwork dependencies

## 2.3 (19.06.2023)
- fix composer.json

## 2.0 (19.06.2023)
- Umstellung auf Bundle-Struktur mit Namespace Srhinow\TeaserManager

## 1.0.15 (29.12.2022)
- TeaserManager.php: fix work with $arrCssID

## 1.0.14 (23.12.2022)
- add featured as class in TeaserManager.php

## 1.0.13 (23.12.2022)
- fix &$GLOBALS['TL_LANG'] in dca-file tl_module.php (PHP8.2)

## 1.0.12 (23.12.2022)
- fix &$GLOBALS['TL_LANG'] in dca-files (PHP8.2)

## 1.0.11 (10.11.2022)
- fix error in TeaserManager.php when custom_pages is checked but no pages is selectet

## 1.0.10 (20.10.2022)
- remove tm_teaser_template from tl_module dca
- fix: remove \Patchwork\Utf8:: from ContentTeaser.php
- add cssID for template
- fix composer.json license string

## 1.0.9 (27.03.2020)
- change license to GPL-3.0-or-later
- ModuleThemeArticle in src/Module verschoben und Namespace von Modules in Module angepasst
- update README.md

## ...

## 1.0.2 (26.06.2017)
- erstellt
